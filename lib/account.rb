class Account

  @@objects = Hash.new
  @@count = 0

  def initialize(name)
    @customer_name = name
    @@count += 1
    @number = @@count
    @balance = 1000
    @@objects[@number] = self
  end

  def self.fetch(number)
    @@objects[number]
  end

  def deposit(amount)
    return nil if (amount <= 0)
    @balance += amount
  end

  def withdraw(amount)
    return nil if not (0..@balance).include? amount
    @balance -= amount
  end
end

