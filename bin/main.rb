require_relative "../lib/account.rb"

while true
  puts "\n-------------------------------"
  puts "Enter your Choice :-"
  puts "1. Create new Customer Account "
  puts "2. Deposit"
  puts "3. Withdraw"
  puts "Q. Quit"
  choice = gets.chomp.upcase
  
  case choice
  when "1"
    print "Enter Customer Name : "
    customer_name = gets.chomp.capitalize
    account = Account.new(customer_name)
    p account

  when "2"
    print "Enter Account Number : "
    number = gets.to_i
    print "Enter Amount : "
    amount = gets.to_f.round(2)
    account = Account.fetch(number)
    account.deposit(amount) if account
    p account

  when "3"
    print "Enter Account Number : "
    number = gets.to_i
    print "Enter Amount : "
    amount = gets.to_f.round(2)
    account = Account.fetch(number)
    account.withdraw(amount) if account
    p account

  when "Q"
    break
  end

end

